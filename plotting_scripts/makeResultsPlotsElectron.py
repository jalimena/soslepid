#!/usr/bin/env python3

from argparse import ArgumentParser
parser = ArgumentParser('makes the plots')
parser.add_argument('inputFile')
parser.add_argument('outputDir')
args=parser.parse_args()

from DeepJetCore.evaluation import makeROCs_async, makeEffPlots_async
import os

os.system('mkdir -p '+args.outputDir)

#create the efficiency plots for different lepton variables
makeEffPlots_async(intextfile=args.inputFile, 
                   name_list=['p>0.7','p>0.8','p>0.9','p>0.95','p>0.98'], 
                   variables='pt', 
                   cutsnum=['isSignal && prob>0.7','isSignal && prob>0.8','isSignal && prob>0.9','isSignal && prob>0.95','isSignal && prob>0.98'],
                   cutsden=[''],
                   colours='auto',
                   outpdffile=args.outputDir+'/efficiency_pt.pdf', 
                   xaxis='p_{T} [GeV]',
                   yaxis='Efficiency',
                   minimum=1e100,maximum=-1e100,
                   nbins=20, SetLogY = False, Xmin = 0.0, Xmax = 250,
                   treename="tree")

makeEffPlots_async(intextfile=args.inputFile, 
                   name_list=['p>0.7','p>0.8','p>0.9','p>0.95','p>0.98'], 
                   variables='eta', 
                   cutsnum=['isSignal && prob>0.7','isSignal && prob>0.8','isSignal && prob>0.9','isSignal && prob>0.95','isSignal && prob>0.98'],
                   cutsden=[''],
                   colours='auto',
                   outpdffile=args.outputDir+'/efficiency_eta.pdf', 
                   xaxis='#eta',
                   yaxis='Efficiency',
                   minimum=1e100,maximum=-1e100,
                   nbins=20, SetLogY = False, Xmin = -2.5, Xmax = 2.5,
                   treename="tree")

makeEffPlots_async(intextfile=args.inputFile, 
                   name_list=['p>0.7','p>0.8','p>0.9','p>0.95','p>0.98'], 
                   variables='pfRelIso03_all', 
                   cutsnum=['isSignal && prob>0.7','isSignal && prob>0.8','isSignal && prob>0.9','isSignal && prob>0.95','isSignal && prob>0.98'],
                   cutsden=[''],
                   colours='auto',
                   outpdffile=args.outputDir+'/efficiency_pfRelIso03.pdf', 
                   xaxis='pfRelIso03',
                   yaxis='Efficiency',
                   minimum=1e100,maximum=-1e100,
                   nbins=20, SetLogY = False, Xmin = 0.0, Xmax = 0.2,
                   treename="tree")

makeEffPlots_async(intextfile=args.inputFile, 
                   name_list=['p>0.7','p>0.8','p>0.9','p>0.95','p>0.98'], 
                   variables='ip3d', 
                   cutsnum=['isSignal && prob>0.7','isSignal && prob>0.8','isSignal && prob>0.9','isSignal && prob>0.95','isSignal && prob>0.98'],
                   cutsden=[''],
                   colours='auto',
                   outpdffile=args.outputDir+'/efficiency_ip3d.pdf', 
                   xaxis='ip3d [cm]',
                   yaxis='Efficiency',
                   minimum=1e100,maximum=-1e100,
                   nbins=20, SetLogY = False, Xmin = 0.0, Xmax = 0.014,
                   treename="tree")

makeEffPlots_async(intextfile=args.inputFile, 
                   name_list=['p>0.7','p>0.8','p>0.9','p>0.95','p>0.98'], 
                   variables='sip3d', 
                   cutsnum=['isSignal && prob>0.7','isSignal && prob>0.8','isSignal && prob>0.9','isSignal && prob>0.95','isSignal && prob>0.98'],
                   cutsden=[''],
                   colours='auto',
                   outpdffile=args.outputDir+'/efficiency_sip3d.pdf', 
                   xaxis='sip3d',
                   yaxis='Efficiency',
                   minimum=1e100,maximum=-1e100,
                   nbins=20, SetLogY = False, Xmin = 0.0, Xmax = 2.5,
                   treename="tree")

#create the ROC curve
makeROCs_async(intextfile=args.inputFile,
               name_list=['electron_ROC'],
               probabilities_list='prob',
               truths_list='isSignal',
               vetos_list='!isSignal',
                    colors_list='auto',
                    outpdffile=args.outputDir+'/electron_roc.pdf',
                    cuts='',
                    cmsstyle=False,
                    firstcomment='',
                    secondcomment='',
                    invalidlist='',
                    extralegend=None, #['solid?udsg','hatched?c'])
                    logY=False,
                    individual=False,
                    xaxis="Signal efficiency",
                    yaxis="Prompt efficiency",
                    nbins=500,
                    treename='tree',
                    xmin=0,
                    yscales=1.0,
                    no_friend_tree=True)

exit()

