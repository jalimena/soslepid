
#! /bin/bash

export SOSLEPID=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P)
export DEEPJETCORE_SUBPACKAGE=$SOSLEPID

cd $SOSLEPID
export PYTHONPATH=$SOSLEPID/modules:$PYTHONPATH
export PYTHONPATH=$SOSLEPID/modules/datastructures:$PYTHONPATH
export PATH=$SOSLEPID/scripts:$PATH

export LD_LIBRARY_PATH=$SOSLEPID/modules/compiled:$LD_LIBRARY_PATH
export PYTHONPATH=$SOSLEPID/modules/compiled:$PYTHONPATH

