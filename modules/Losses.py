
# Define custom losses here and add them to the global_loss_list dict (important!)
global_loss_list = {}

import tensorflow as tf

def null_loss(truth, predicted):
    return tf.reduce_sum(truth-predicted)*0.


global_loss_list['null_loss']=null_loss
